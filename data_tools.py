#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
import numpy as np
import pandas as pd
import datetime
import time
import pytz
import re
from os import listdir
from os.path import isfile, join

"""
this is really similiar to mysql_tools.py; I changed the insertinto... method for 
sql server into text file format. Each text file has a title of stationID + type
+ txt. So far I did not change the getter and setter, which is written for sql.
"""

def datetime_tz(year, month, day, hour=0, minute=0, second=0, tz='UTC'):
    dt_naive = datetime.datetime(year, month, day, hour, minute, second)
    tzinfo = pytz.timezone(tz)
    return tzinfo.localize(dt_naive)


def epoch_to_datetime(epoch, tz='UTC'):
    """Return datetime with associated timezone."""
    dt_utc = (datetime_tz(1970, 1, 1, tz='UTC') +
              datetime.timedelta(seconds=epoch))
    tzinfo = pytz.timezone(tz)
    return dt_utc.astimezone(tzinfo)

class TextObject:
    def __init__(self, tz='+00:00', Data_Path="/home/pi/data/"):
        self.set_session_tz(tz)
        self.Data_Path = Data_Path

    def __del__(self):
        try:
            self.close()
        except:
            pass

    def __exit__(self):
        try:
            self.close()
        except:
            pass

    def set_session_tz(self, tz):
        """Sets timezone for this MySQL session.
        This affects the timestamp strings shown by deviceTime and receiveTime.
        Does NOT affect UNIX_TIMESTAMP(deviceTime), UNIX_TIMESTAMP(receiveTime)

        Might not be needed. Depends what you're doing with the data.
        """
        print('[CONFIG] Setting session timezone to: {}'.format(tz))

    def getStationTZ(self):
        station = pd.read_csv("/home/pi/config/config.csv")
        timezone = station['timezone'].values[0]
        return timezone

    def getStationName(self):
        station = pd.read_csv("/home/pi/config/config.csv")
        name = station['nickname'].values[0]
        return name

# ---------------------------------------------------------------------------
#       INJECTION-RELATED METHODS
# ---------------------------------------------------------------------------
    def insertIntoDosenet(self, stationID, cpm, cpm_error, error_flag, deviceTime=None, **kwargs):
        """
        Create text file with the name of stationID and the type, dosimeter.

        Data are written in the order of device time, stationID, cpm, cpm_error, error flag

        Insert a row of dosimeter data into the dosnet table

        NOTE that the receiveTime is not included since that is assigned my
        the MySQL default value of CURRENT_TIMESTAMP
        """
        if (not isinstance(deviceTime, int) and
                not isinstance(deviceTime, float)):
            if deviceTime is not None:
                print('Warning: received non-numeric deviceTime! Ignoring')
            deviceTime = time.time()
        try:
            deviceTimeUTC = epoch_to_datetime(deviceTime).strftime('%Y-%m-%d %H:%M:%S%z')
            deviceTimeLocal = epoch_to_datetime(deviceTime, self.getStationTZ()).strftime('%Y-%m-%d %H:%M:%S%z')
            file_path = self.Data_Path + self.getStationName() + ".csv"
            dosimeterfile = open(file_path, "a+")
            dosimeterfile.write("{},{},{},{},{},{}\n".format(deviceTimeUTC, deviceTimeLocal, deviceTime, cpm, cpm_error, error_flag))
            dosimeterfile.close()
        except Exception as e:
            print("ERROR: failed to write pocket geiger data to file!")
            print(e)
            pass

    def insertIntoAQ(self, stationID, oneMicron, twoPointFiveMicron, tenMicron, error_flag, deviceTime = None, **kwargs):
        """
        Create text file with the name of stationID and the type, AQ.

        Data are written in the order of device time, stationID, oneMicron,
        twoPointFiveMicron, TenMicron, error flag

        Insert a row of Air Quality data into the Air Quality table
        """
        if (not isinstance(deviceTime, int) and
                not isinstance(deviceTime, float)):
            if deviceTime is not None:
                print('Warning: received non-numeric deviceTime! Ignoring')
            deviceTime = time.time()
        try:
            deviceTimeUTC = epoch_to_datetime(deviceTime).strftime('%Y-%m-%d %H:%M:%S%z')
            deviceTimeLocal = epoch_to_datetime(deviceTime, self.getStationTZ()).strftime('%Y-%m-%d %H:%M:%S%z')
            aqfile = open(self.Data_Path + self.getStationName() + "_aq.csv", "a+")
            aqfile.write("{},{},{},{},{},{},{}\n".format(deviceTimeUTC, deviceTimeLocal, deviceTime, oneMicron, twoPointFiveMicron,
            tenMicron, error_flag))
            aqfile.close()
        except Exception as e:
            print("ERROR: failed to write AQ data to file!")
            print(e)
            pass

    def insertIntoCO2(self, stationID, co2_ppm, noise, error_flag, deviceTime = None, **kwargs):
        """
        Create text file with the name of stationID and the type, CO2.

        Data are written in the order of device time, stationID, co2_ppm, noise, error flag

        Insert a row of CO2 data into the CO2 table
        """
        if (not isinstance(deviceTime, int) and
                not isinstance(deviceTime, float)):
            if deviceTime is not None:
                print('Warning: received non-numeric deviceTime! Ignoring')
            deviceTime = time.time()
        deviceTimeUTC = epoch_to_datetime(deviceTime).strftime('%Y-%m-%d %H:%M:%S%z')
        deviceTimeLocal = epoch_to_datetime(deviceTime, self.getStationTZ()).strftime('%Y-%m-%d %H:%M:%S%z')
        try:
            co2file = open(self.Data_Path + self.getStationName() + "_adc.csv", "a+")
            co2file.write("{},{},{},{},{},{}\n".format(deviceTimeUTC, deviceTimeLocal, deviceTime, co2_ppm, noise, error_flag))
            co2file.close()
        except:
            pass

    def insertIntoWeather(self, stationID, temperature, pressure, humidity, error_flag, deviceTime = None, **kwargs):
        """
        Create text file with the name of stationID and the type, weather.

        Data are written in the order of device time, stationID, temperature, pressure, humidity, error_flag

        Insert a row of Weather data into the Weather table
        """
        print("Inserting into weather data!")
        if (not isinstance(deviceTime, int) and
                not isinstance(deviceTime, float)):
            if deviceTime is not None:
                print('Warning: received non-numeric deviceTime! Ignoring')
            deviceTime = time.time()
        try:
            deviceTimeUTC = epoch_to_datetime(deviceTime).strftime('%Y-%m-%d %H:%M:%S%z')
            deviceTimeLocal = epoch_to_datetime(deviceTime, self.getStationTZ()).strftime('%Y-%m-%d %H:%M:%S%z')
            weatherfile = open(self.Data_Path + self.getStationName() + "_weather.csv", "a+")
            weatherfile.write("{},{},{},{},{},{},{}\n".format(deviceTimeUTC, deviceTimeLocal ,deviceTime, temperature, pressure,
            humidity, error_flag))
            weatherfile.close()
        except Exception as e:
            print("ERROR: failed to write weather data to file!")
            print(e)
            pass

    def insertIntoD3S(self, stationID, spectrum, error_flag, deviceTime = None, **kwargs):
        """
        Create text file with the name of stationID and the type, D3S.

        Data are written in the order of device time, stationID, count, spectrum_blob, error flag

        Insert a row of D3S data into the d3s table.
        """
        spectrum = np.array(spectrum, dtype=np.uint8)
        rebin_array = spectrum.reshape(len(spectrum) // 4, 4).sum(1)
        spectrum_string = ",".join(map(str, rebin_array.tolist()))
        cpm = sum(rebin_array) / 5
        cpm_error = np.sqrt(sum(rebin_array)) / 5
        keV_per_ch = 2.57
        if (not isinstance(deviceTime, int) and
                not isinstance(deviceTime, float)):
            if deviceTime is not None:
                print('Warning: received non-numeric deviceTime! Ignoring')
            deviceTime = time.time()
        deviceTimeUTC = epoch_to_datetime(deviceTime).strftime('%Y-%m-%d %H:%M:%S%z')
        deviceTimeLocal = epoch_to_datetime(deviceTime, self.getStationTZ()).strftime('%Y-%m-%d %H:%M:%S%z')
        try:
            d3sfile = open(self.Data_Path + self.getStationName() + "_d3s.csv", "a+")
            d3sfile.write("{},{},{},{},{},{},{},{}\n".format(deviceTimeUTC, deviceTimeLocal, deviceTime, cpm, cpm_error, keV_per_ch, spectrum_string, error_flag))
            d3sfile.close()
        except:
            pass

    def insertIntoLog(self, stationID, msgCode, msgText, deviceTime = None, **kwargs):
        """
        Create text file with the name of stationID and the type, dosimeter.

        Data are written in the order of magCode, msgText error flag

        Insert a log message into the stationlog table.
        """
        if (not isinstance(deviceTime, int) and
                not isinstance(deviceTime, float)):
            if deviceTime is not None:
                print('Warning: received non-numeric deviceTime! Ignoring')
            deviceTime = time.time()
        deviceTimeUTC = epoch_to_datetime(deviceTime).strftime('%Y-%m-%d %H:%M:%S%z')
        deviceTimeLocal = epoch_to_datetime(deviceTime, self.getStationTZ()).strftime('%Y-%m-%d %H:%M:%S%z')
        try:
            logfile = open(self.Data_Path + self.getStationName() + "_log.csv", "a+")
            logfile.write("{},{},{},{},{}\n".format(deviceTimeUTC, deviceTimeLocal, deviceTime, msgCode, msgText))
            logfile.close()
        except:
            pass

    def inject(self, data, verbose=False):
        """Authenticate the data packet and then insert into database"""
        tic = time.time()
        self.insertIntoDosenet(**data)
        toc = time.time()
        if verbose:
            print('insertIntoDosenet took {} ms'.format((tic - toc) * 1000))

