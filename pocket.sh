#! /bin/sh

HOME=/home/pi
DOSENET=$HOME/dosenet-raspberrypi

LOG=/tmp/pocket_manager.log

os_info=$(cat /etc/os-release)

VERSION_ID=$(echo "$os_info" | grep -oP 'VERSION_ID="\K[^"]+')

if [ "$VERSION_ID" -gt "10" ]; then
  PYTHON_PATH=$HOME/dosenet/bin/python
else
  PYTHON_PATH=python3
fi

case "$1" in
  start)
    echo "Starting pocket Geiger script" > $LOG
    echo "Running $PYTHON_PATH" > $LOG
    sudo $PYTHON_PATH $DOSENET/managers.py --sensor 1 --log --logfile $LOG >>$LOG 2>&1
    echo "Finished pocket Geiger script" >> $LOG
    exit 0
    ;;
  test)
    echo "Starting pocket Geiger script in test mode" > $LOG
    sudo $PYTHON_PATH $DOSENET/managers.py --sensor 1 --test --log --logfile $LOG >> $LOG 2>&1
    ;;
  stop)
    echo "Stopping pocket Geiger script" >> $LOG
    sudo pkill -SIGQUIT -f managers.py
    ;;
 *)
    echo "Usage: /home/pi/dosenet-raspberrypi/pocket.sh {start|test|stop}"
    exit 1
    ;;
esac
