#! /bin/sh

HOME=/home/pi
DOSENET=$HOME/dosenet-raspberrypi

LOG=/tmp/weather_manager.log

os_info=$(cat /etc/os-release)

VERSION_ID=$(echo "$os_info" | grep -oP 'VERSION_ID="\K[^"]+')

if [ "$VERSION_ID" -gt "10" ]; then
  PYTHON_PATH=$HOME/dosenet/bin/python
else
  PYTHON_PATH=python3
fi

case "$1" in
  start)
    echo "Starting weather sensor script"  > $LOG
    sudo ${PYTHON_PATH} $DOSENET/managers.py --sensor 5 --log --logfile $LOG >>$LOG 2>&1
    echo "Finished weather sensor script"  >> $LOG
    ;;
  test)
    echo "Starting weather sensor script in test mode" > $LOG
    sudo ${PYTHON_PATH} $DOSENET/managers.py --sensor 5 --test --log --logfile $LOG >> $LOG 2>&1
    ;;
  stop)
    echo "Stopping weather sensor script" >> $LOG
    sudo pkill -SIGQUIT -f managers.py
    ;;
 *)
    echo "Usage: /home/pi/dosenet-raspberrypi/weather.sh {start|test|stop}"
    exit 1
    ;;
esac

exit 0
