import datetime
import time
import ast
import csv
import os
import errno

from auxiliaries import datetime_from_epoch
from auxiliaries import set_verbosity
from globalvalues import DEFAULT_DATA_BACKLOG_FILE
from globalvalues import DEFAULT_DATA_BACKLOG_FILE_D3S
from globalvalues import DEFAULT_DATA_BACKLOG_FILE_AQ
from globalvalues import DEFAULT_DATA_BACKLOG_FILE_CO2
from globalvalues import DEFAULT_DATA_BACKLOG_FILE_WEATHER
from globalvalues import NETWORK_LED_BLINK_PERIOD_S, NETWORK_LED_BLINK_LOST_CONNECTION

from globalvalues import CPM_DISPLAY_TEXT
from globalvalues import SPECTRA_DISPLAY_TEXT
from globalvalues import AQ_PM_DISPLAY_TEXT, AQ_P_DISPLAY_TEXT
from globalvalues import CO2_DISPLAY_TEXT
from globalvalues import WEATHER_DISPLAY_TEXT
from globalvalues import TIME_DISPLAY_TEXT
from globalvalues import SINGLE_BREAK_LINE, DOUBLE_BREAK_LINE
from globalvalues import SENSOR_SHORT_NAME

from globalvalues import FLUSH_PAUSE_S
from globalvalues import strf
from collections import deque

class Data_Handler(object):
    """
    Main Data Handler class that deals with the generic
    data from any of the sensors.

    The more specifc data handling processes are dealt with in
    the sub-classes below.
    """

    def __init__(self,
                 manager=None,
                 verbosity=1,
                 logfile=None,
                 ):
        self.send_fail = False

        self.v = verbosity
        if manager and logfile is None:
            set_verbosity(self, logfile=manager.logfile)
        else:
            set_verbosity(self, logfile=logfile)

        self.first_run = True

        self.manager = manager
        self.queue = deque('')

        self.stype = SENSOR_SHORT_NAME[self.manager.sensor_type-1]

    def test_send(self, **kwargs):
        """
        Test mode
        """
        self.vprint(
            1, ANSI_RED + " * Test mode, not sending to server * " +
            ANSI_RESET)

    def no_config_send(self, **kwargs):
        """
        Configuration file not present
        """
        self.vprint(1, "Missing config file, default file names, not sending to dropbox")

    def regular_send(self, this_end, **kwargs):
        """
        Normal send - meaning standard writing of data to file
        """
        if self.manager.sensor_type == 1:
            cpm, cpm_err = kwargs.get('cpm'), kwargs.get('cpm_err')
            if self.led and self.first_run:
                self.first_run = False
                if self.led.blinker:
                    self.led.stop_blink()
                self.led.on()
            self.manager.writer.send_cpm(self.stype, this_end, cpm, cpm_err)

        if self.manager.sensor_type == 2:
            spectra = kwargs.get('spectra')
            self.manager.writer.send_spectra_D3S(self.stype, this_end, spectra)

        if self.manager.sensor_type in [3,4,5]:
            average_data = kwargs.get('average_data')
            self.manager.writer.send_data_Env(self.stype, this_end, average_data)


    def main(self, datalog, this_start, this_end, **kwargs):
        """
        Determines how to handle the sensor data.
        """
        start_text = datetime_from_epoch(this_start).strftime(strf)
        end_text = datetime_from_epoch(this_end).strftime(strf)
        date = str(datetime.date.today())
        display_data = []
        if self.manager.sensor_type == 1:
            cpm, cpm_err = kwargs.get('cpm'), kwargs.get('cpm_err')
            counts = kwargs.get('counts')
            self.vprint(
                1, SINGLE_BREAK_LINE)
            self.vprint(
                1, TIME_DISPLAY_TEXT.format(
                    start_time=start_text,
                    end_time=end_text,
                    date=date))
            self.vprint(
                1, CPM_DISPLAY_TEXT.format(
                    counts=counts,
                    cpm=cpm,
                    cpm_err=cpm_err))
            self.vprint(
                1, SINGLE_BREAK_LINE)
            self.manager.data_log(datalog, cpm=cpm, cpm_err=cpm_err)
            display_data = [cpm, cpm_err]

        if self.manager.sensor_type == 2:
            spectra = kwargs.get('spectra')
            calibrationlog = kwargs.get('calibrationlog')
            self.vprint(
                1, SINGLE_BREAK_LINE)
            self.vprint(
                1, TIME_DISPLAY_TEXT.format(
                    start_time=start_text,
                    end_time=end_text,
                    date=date))
            self.vprint(
                1, SPECTRA_DISPLAY_TEXT.format(
                    total_counts=sum(spectra)))
            self.vprint(
                1, SINGLE_BREAK_LINE)

            self.manager.data_log(datalog, spectra=spectra)
            self.manager.calibration_log(calibrationlog, spectra)
            display_data = [sum(spectra)/float(self.manager.interval)]

        if self.manager.sensor_type == 3:
            average_data = kwargs.get('average_data')
            self.vprint(
                1, SINGLE_BREAK_LINE)
            self.vprint(
                1, TIME_DISPLAY_TEXT.format(
                    start_time=start_text,
                    end_time=end_text,
                    date=date))
            for i in range(3):
            	self.vprint(
                    1, AQ_PM_DISPLAY_TEXT.format(
                        variable=self.variables[i],
                        avg_data=average_data[i]))
            for i in range(3, 9):
            	self.vprint(
                    1, AQ_P_DISPLAY_TEXT.format(
                        variable=self.variables[i],
                        avg_data=average_data[i]))
            self.vprint(
                1, SINGLE_BREAK_LINE)

            self.manager.data_log(datalog, average_data=average_data)
            display_data = average_data

        if self.manager.sensor_type == 4:
            average_data = kwargs.get('average_data')
            self.vprint(
                1, SINGLE_BREAK_LINE)
            self.vprint(
                1, TIME_DISPLAY_TEXT.format(
                    start_time=start_text,
                    end_time=end_text,
                    date=date))
            for i in range(len(self.variables)):
                self.vprint(
                    1, CO2_DISPLAY_TEXT.format(
                        variable=self.variables[i],
                        data=average_data[i]))
            self.vprint(
                1, SINGLE_BREAK_LINE)

            self.manager.data_log(datalog, average_data=average_data)
            display_data = average_data

        if self.manager.sensor_type == 5:
            average_data = kwargs.get('average_data')
            self.vprint(
                1, SINGLE_BREAK_LINE)
            self.vprint(
                1, TIME_DISPLAY_TEXT.format(
                    start_time=start_text,
                    end_time=end_text,
                    date=date))
            for i in range(len(self.variables)):
                self.vprint(
                    1, WEATHER_DISPLAY_TEXT.format(
                        variable=self.variables[i],
                        unit=self.variables_units[i],
                        data=average_data[i]))
            self.vprint(
                1, SINGLE_BREAK_LINE)

            self.manager.data_log(datalog, average_data=average_data)
            display_data = average_data

        if self.manager.oled:
            self.manager.oled_send(display_data)

        ## TO-DO: update testing for new dropbox framework
        '''
        if self.manager.test:
            if self.manager.sensor_type == 1:
                self.send_to_memory(cpm=cpm, cpm_err=cpm_err)
            elif self.manager.sensor_type == 2:
                self.send_to_memory(spectra=spectra)
            elif self.manager.sensor_type in [3,4,5]:
                self.send_to_memory(average_data=average_data)
        '''

        if not self.manager.test:
            try:
                if self.manager.sensor_type == 1:
                    self.regular_send(this_end, cpm=cpm, cpm_err=cpm_err)
                if self.manager.sensor_type == 2:
                    self.regular_send(this_end, spectra=spectra)
                if self.manager.sensor_type == 3 or \
                    self.manager.sensor_type == 4 or self.manager.sensor_type == 5:
                    self.regular_send(this_end, average_data=average_data)
            except Exception as e:
                if self.manager.sensor_type == 1:
                    if self.send_fail:
                        if not self.manager.small_board:
                            self.led.stop_blink()
                            self.led.off()
                    if not self.send_fail:
                        self.send_fail = True
                        if not self.manager.small_board:
                            self.led.start_blink(interval=self.blink_period_lost_connection)

class Data_Handler_Pocket(Data_Handler):
    """
    Sub data handler for the Pocket Geiger sensor.

    Any specific variables to this sensor will be
    defined in this sub-class.
    """

    def __init__(self,
                 network_led=None,
                 **kwargs):

        super(Data_Handler_Pocket, self).__init__(**kwargs)

        self.blink_period_s = NETWORK_LED_BLINK_PERIOD_S
        self.blink_period_lost_connection = NETWORK_LED_BLINK_LOST_CONNECTION
        self.led = network_led

class Data_Handler_D3S(Data_Handler):
    """
    Sub data handler for the D3S sensor.

    Any specific variables to this sensor will be
    defined in this sub-class.
    """

    def __init__(self,
                 **kwargs):

        super(Data_Handler_D3S, self).__init__(**kwargs)

class Data_Handler_AQ(Data_Handler):
    """
    Sub data handler for the Air Quality sensor.

    Any specific variables to this sensor will be
    defined in this sub-class.
    """

    def __init__(self,
                 variables=None,
                 **kwargs):

        self.variables = variables

        super(Data_Handler_AQ, self).__init__(**kwargs)

class Data_Handler_CO2(Data_Handler):
    """
    Sub data handler for the CO2 sensor.

    Any specific variables to this sensor will be
    defined in this sub-class.
    """

    def __init__(self,
                 variables=None,
                 **kwargs):

        self.variables = variables

        super(Data_Handler_CO2, self).__init__(**kwargs)

class Data_Handler_Weather(Data_Handler):
    """
    Sub data handler for the weather sensor.

    Any specific variables to this sensor will be
    defined in this sub-class.
    """

    def __init__(self,
                 variables=None,
                 variables_units=None,
                 **kwargs):

        self.variables = variables
        self.variables_units = variables_units

        super(Data_Handler_Weather, self).__init__(**kwargs)
