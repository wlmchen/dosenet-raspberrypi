#! /bin/sh

# setup script for the dosenet systems

sudo apt-get update
sudo apt-get upgrade
sudo apt-get install build-essential tk-dev libncurses5-dev libncursesw5-dev libreadline6-dev libdb5.3-dev libgdbm-dev libsqlite3-dev libssl-dev libbz2-dev libexpat1-dev liblzma-dev zlib1g-dev libffi-dev -y
sudo apt install dnsmasq hostapd

sudo apt-get clean
sudo apt-get autoremove

sudo apt-get install python3-full
python -m venv dosenet
source dosenet/bin/activate
pip install numpy pandas pika dropbox datetime matplotlib pyzmq pyserial
pip install adafruit-circuitpython-mcp3xxx 

if [ "$1" = "old" ]
    then 
        pip install adafruit-circuitpython-bme280 
        pip install RPi.GPIO
else
    pip install rpi-lgpio
    pip install adafruit-circuitpython-bme680 
    pip install adafruit-circuitpython-pm25
fi

echo 'interface wlan1' | sudo tee -a '/etc/dhcpcd.conf'
echo 'static ip_address=192.168.4.1/24' | sudo tee -a '/etc/dhcpcd.conf'
echo 'nohook wpa_supplicant' | sudo tee -a '/etc/dhcpcd.conf'

echo 'interface=wlan1' | sudo tee -a '/etc/dnsmasq.conf'
echo 'dhcp-range=192.168.4.2,192.168.4.20,255.255.255.0,24h' | sudo tee -a '/etc/dnsmasq.conf'

echo 'interface=wlan1' | sudo tee -a '/etc/hostapd/hostapd.conf'
echo 'driver=nl80211' | sudo tee -a '/etc/hostapd/hostapd.conf'
echo 'ssid=RPiServer' | sudo tee -a '/etc/hostapd/hostapd.conf'
echo 'hw_mode=g' | sudo tee -a '/etc/hostapd/hostapd.conf'
echo 'channel=7' | sudo tee -a '/etc/hostapd/hostapd.conf'
echo 'wmm_enabled=0' | sudo tee -a '/etc/hostapd/hostapd.conf'
echo 'macaddr_acl=0' | sudo tee -a '/etc/hostapd/hostapd.conf'
echo 'auth_algs=1' | sudo tee -a '/etc/hostapd/hostapd.conf'
echo 'ignore_broadcast_ssid=0' | sudo tee -a '/etc/hostapd/hostapd.conf'
echo 'wpa=2' | sudo tee -a '/etc/hostapd/hostapd.conf'
echo 'wpa_passphrase=servingpiisTasty!' | sudo tee -a '/etc/hostapd/hostapd.conf'
echo 'wpa_key_mgmt=WPA-PSK' | sudo tee -a '/etc/hostapd/hostapd.conf'
echo 'wpa_pairwise=TKIP' | sudo tee -a '/etc/hostapd/hostapd.conf'
echo 'rsn_pairwise=CCMP' | sudo tee -a '/etc/hostapd/hostapd.conf'

echo 'DAEMON_CONF="/etc/hostapd/hostapd.conf"' | sudo tee -a '/etc/default/hostapd'

sudo systemctl unmask hostapd
sudo systemctl enable hostapd
sudo systemctl enable dnsmasq

sudo reboot
