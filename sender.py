# -*- coding: utf-8 -*-
"""
If you call sender.py from the command line, it will send 3 test packets.
See argparse for options.

Otherwise, this module is loaded to provide the ServerSender class.
"""

from __future__ import print_function

import glob
import os
import dropbox
import argparse
import time
import numpy as np
from contextlib import closing

from auxiliaries import set_verbosity, Config
from globalvalues import NETWORK_LED_BLINK_PERIOD_S
from globalvalues import DATA_PATH, DEFAULT_CONFIG

PING_TIMEOUT = 5
D3S_PREPEND_STR = '{:05d}'


class ServerSender(object):
    """
    Provides ServerSender.send_cpm() for sending UDP packets to the DoseNet
    server.
    """

    def __init__(self,
                 manager=None,
                 config=None,
                 verbosity=1,
                 logfile=None,
                 mode=None,
                 ):
        """
        network_status, config, publickey, aes loaded from manager
          if not provided.
        address and port take system defaults, although without config and
          publickey, address and port will not be used.
        """
        self.v = verbosity
        if manager and logfile is None:
            set_verbosity(self, logfile=manager.logfile)
        else:
            set_verbosity(self, logfile=logfile)

        self.handle_input(
            manager, mode, config)

    def handle_input(
            self, manager, mode, config):

        # TODO: this stuff is messy. Is there a cleaner way using exceptions?
        if manager is None:
            self.vprint(1, 'ServerSender starting without Manager object')
        self.manager = manager

        if mode is None:
            self.mode = 'upload'

        if config is None:
            if manager is None:
                self.vprint(1, 'ServerSender starting without config file')
                self.config = None
            else:
                self.config = manager.config
        else:
            self.config = config

    def send_data(self):
        """
        Send data according to self.mode
        """

        self.vprint(3, 'Trying to send data by {}'.format(self.mode))
        dbx = self.config.dbx
        data_files = glob.glob(DATA_PATH + "*")

        for file in data_files:
            file_path = file
            destination_path = '/data/'+file_path.rsplit('/',1)[-1]
            f = open(file_path,'rb')
            file_size = os.path.getsize(file_path)

            CHUNK_SIZE = 4 * 1024 * 1024

            try:
                if file_size <= CHUNK_SIZE:
                    dbx.files_upload(f.read(), destination_path, mode=dropbox.files.WriteMode.overwrite)

                else:
                    upload_session_start_result = dbx.files_upload_session_start(f.read(CHUNK_SIZE)) #read 4mb of the file, start session
                    cursor = dropbox.files.UploadSessionCursor(session_id=upload_session_start_result.session_id,offset=f.tell())
                    # UploadSessorCursor contains the upload session ID and the offset.
                    # I'm guessing they're setting the initial offset to 0mb (right?) through f.tell()
                    # f.tell(): returns an integer giving the file object’s current position in the file represented as number of bytes from the beginning of the file.

                    commit = dropbox.files.CommitInfo(path=destination_path,mode=dropbox.files.WriteMode.overwrite)
                    #Contains the path and other optional modifiers for the future upload commit.

                    while f.tell() < file_size:
                        b_left_last = 100.0
                        if ((file_size - f.tell()) <= CHUNK_SIZE):
                            #if remaining filesize is less than 4mb
                            print('')
                            print(dbx.files_upload_session_finish(f.read(CHUNK_SIZE),cursor,commit).name)
                            print('complete\n\n')
                            #Finish the upload session and save the uploaded data to the given file path.
                        else:
                            dbx.files_upload_session_append_v2(f.read(CHUNK_SIZE),cursor)
                            #append more data to an upload session
                            cursor.offset = f.tell()
                            #offset updated to new byte position in file
                            b_left = round(((file_size-f.tell())/file_size),2)*100
                            b_left = str(b_left)+"%" if b_left < 1000 else 0
                            #fun percentage remaining printing thing but only print sometimes
                            if (b_left_last - b_left) > 20:
                                print("\r       {} remaining....".format(b_left))
                            b_left_last = b_left
            except Exception as e:
                print("ERROR: upload of ",file_path,'failed!')
                #print("ERROR: upload of",dbx.files_upload_session_finish(f.read(CHUNK_SIZE),cursor,commit).name,'failed!')
                print("")
                print(e)
                print("")
                pass

    ### TO-DO: Update to use ping to check basic network
    '''
    def handle_return_packet(self, received):
        """
        Extracts the git tag from sender and puts it into a list.
        """
        try:
            received = [x.strip() for x in received.split(',')]
            branch = received[0]
            if int(received[1]) == 0:
                flag = False
            else:
                flag = True
            assert len(received) == 2
        except (AttributeError, IndexError, ValueError, AssertionError):
            return None, None
        else:
            return branch, flag
    '''


if __name__ == '__main__':
    # send a test log entry
    parser = argparse.ArgumentParser(
        description='Sender for UDP/TCP data packets. ' +
        'Normally called from manager.py. ' +
        'Called directly, it will send a log message to the server.')
    parser.add_argument('--mode', '-n', choices=['upload', 'test'],
                        default='upload',
                        help='Network protocol to use')
    parser.add_argument('--config', '-g', type=str, default=DEFAULT_CONFIG,
                        help='config file location')
    parser.add_argument('--msgcode', '-c', type=int, default=0,
                        help='message code for log')
    parser.add_argument('--message', '-m', type=str,
                        default='ServerSender test',
                        help='message text for log')
    args = parser.parse_args()
